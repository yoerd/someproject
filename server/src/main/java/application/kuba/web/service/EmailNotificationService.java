package application.kuba.web.service;

import org.springframework.stereotype.Service;

@Service
public interface EmailNotificationService {

    void sendEmailNotification(String email, String message);
}
