package application.kuba.web.service.impl;

import application.kuba.web.service.EmailNotificationService;
import org.springframework.stereotype.Service;

@Service
public class EmailNotificationServiceMock implements EmailNotificationService {
    @Override
    public void sendEmailNotification(String email, String message) {
        return;
    }
}
