package application.kuba.web.config;

import application.kuba.web.interceptors.PerformanceLogAspect;
import application.kuba.web.service.EmailNotificationService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;


@Configuration
@EnableAspectJAutoProxy
public class AspectJConfig {


    private EmailNotificationService emailNotificationService;

    public AspectJConfig( EmailNotificationService emailNotificationService) {
        this.emailNotificationService = emailNotificationService;
    }

    @Bean
    public PerformanceLogAspect performanceAspect(){
        return new PerformanceLogAspect();
    }



}
