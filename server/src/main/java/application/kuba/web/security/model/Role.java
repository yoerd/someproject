package application.kuba.web.security.model;

public enum Role {
    ROLE_ADMIN, ROLE_USER
}
