package application.kuba.web.films.mapper;

import application.kuba.web.films.model.FetchData;
import application.kuba.web.films.model.Film;
import application.kuba.web.films.model.FilmListModel;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FilmsMapper {

    String FETCH_PRODUCTS_QUERY =
            "select p.id, p.name, p.descriptions " +
            "from films p " +
            "where p.name like #{searchText} " +
            "group by p.id " +
            "order by p.id asc " +
            "LIMIT #{limit} " +
            "OFFSET #{offset}";

    @Insert("insert into films(name,descriptions,opinion,director,category_fk) " +
            "values(#{name},#{descriptions},#{opinion},#{director},#{category.id})")
    @Options(useGeneratedKeys=true)
    int insertProduct(Film film);

    @Select("select * from films where id = #{id}")
    Film getProduct(long id);

    @Select(FETCH_PRODUCTS_QUERY)
    List<FilmListModel> fetchProducts(FetchData fetchData);

    @Update("update films set name = #{name}, descriptions = #{descriptions}, " +
            "opinion = #{opinion}, director = #{director}, category_fk = #{category.id} where id = #{id}")
    int updateProduct(Film film);
}
