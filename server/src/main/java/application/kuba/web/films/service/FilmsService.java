package application.kuba.web.films.service;

import application.kuba.web.films.model.FetchData;
import application.kuba.web.films.model.Film;
import application.kuba.web.films.model.FilmListModel;

import java.util.List;

public interface FilmsService {

    int createProduct(Film film);

    int updateProduct(Film film);

    Film getProduct(long id);

    List<FilmListModel> fetchProducts(FetchData fetchData);
}
