package application.kuba.web.films.controller;

import application.kuba.web.films.model.FetchData;
import application.kuba.web.films.model.Film;
import application.kuba.web.films.model.FilmListModel;
import application.kuba.web.films.service.FilmsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
public class FilmsController {

    private FilmsService filmsService;

    public FilmsController(FilmsService filmsService) {
        this.filmsService = filmsService;
    }

    @RequestMapping(value = "/film", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> creteNewFilm(@RequestParam("film") @Valid String productStr)
            throws AuthenticationException, IOException {

        Film film = new ObjectMapper().readValue(productStr, Film.class);

        if (!film.valid()) {
            return new ResponseEntity<>("NOT_VALID", HttpStatus.BAD_REQUEST);
        }

        if (film.getId() > 0) {
            return updateFilmAndGetResponse(film);
        } else {
            return createFilmAndGetResponse(film);
        }
    }

    private ResponseEntity<?> createFilmAndGetResponse(Film film) throws IOException {
        int updatedCount = filmsService.createProduct(film);


        if (updatedCount > 0) {
            return new ResponseEntity<>(film, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }


    private ResponseEntity<?> updateFilmAndGetResponse(Film film) throws IOException {
        int updatedCount = filmsService.updateProduct(film);

        if (updatedCount > 0) {
            return new ResponseEntity<>(film, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "public/film/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getFilm(@PathVariable("id") long id) {
        Film film = filmsService.getProduct(id);

        if (film != null) {
            return new ResponseEntity<>(film, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "public/films", method = RequestMethod.POST)
    public ResponseEntity<?> fetchFilms(@RequestBody FetchData fetchData) {
        List<FilmListModel> products = filmsService.fetchProducts(fetchData);

        if (products != null) {
            return new ResponseEntity<>(products, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


}
