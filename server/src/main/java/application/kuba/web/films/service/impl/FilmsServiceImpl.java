package application.kuba.web.films.service.impl;

import application.kuba.web.categories.mapper.CategoryMapper;
import application.kuba.web.films.mapper.FilmsMapper;
import application.kuba.web.films.model.FetchData;
import application.kuba.web.films.model.Film;
import application.kuba.web.films.model.FilmListModel;
import application.kuba.web.films.service.FilmsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class FilmsServiceImpl implements FilmsService {

    private FilmsMapper filmsMapper;
    private CategoryMapper categoryMapper;

    public FilmsServiceImpl(FilmsMapper filmsMapper, CategoryMapper categoryMapper) {
        this.filmsMapper = filmsMapper;
        this.categoryMapper = categoryMapper;

    }

    @Override
    public int createProduct(Film film) {
        return filmsMapper.insertProduct(film);
    }

    @Override
    public int updateProduct(Film film) {
        return filmsMapper.updateProduct(film);
    }

    @Override
    public Film getProduct(long id) {
        Film film = filmsMapper.getProduct(id);
        if (film == null) {
            return null;
        }

        populateProductRlationData(film);
        return film;
    }

    private void populateProductRlationData(Film film) {
        film.setCategory(categoryMapper.getTypeByProductId(film.getId()));
    }

    @Override
    public List<FilmListModel> fetchProducts(FetchData fetchData) {
        fetchData.setSearchText("%" + fetchData.getSearchText() + "%");
        return filmsMapper.fetchProducts(fetchData);
    }


}
