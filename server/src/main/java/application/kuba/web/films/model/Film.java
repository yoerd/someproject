package application.kuba.web.films.model;

import application.kuba.web.categories.model.Category;
import org.springframework.util.StringUtils;

public class Film {

    private long id;
    private String name;
    private String descriptions;
    private Category category;
    private String opinion;
    private String director;


    public Film() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getOpinion() {
        return opinion;
    }

    public void setOpinion(String opinion) {
        this.opinion = opinion;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public boolean valid() {
        return StringUtils.hasText(name)
                && category != null;
    }
}
