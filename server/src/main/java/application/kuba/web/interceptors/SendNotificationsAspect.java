package application.kuba.web.interceptors;

import application.kuba.web.service.EmailNotificationService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class SendNotificationsAspect {

    public static final String MESSAGE = "Nowa aktywność dla produktu który oceniłeś lub skomentowałeś";
    private EmailNotificationService emailNotificationService;

    public SendNotificationsAspect( EmailNotificationService emailNotificationService) {
        this.emailNotificationService = emailNotificationService;
    }

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Pointcut("@annotation(SendNotifications)")
    public void allMethodWithAnnotation() {}

    @Pointcut("execution(public * *(..))")
    public void publicMethod() {}

    @AfterReturning(value = "publicMethod() && allMethodWithAnnotation()")
    public void logExecutionTime(JoinPoint joinPoint) {
        //List<String> allCommentingUsers = commentsService.getCommentingUsers(result.getProductId());
        //List<String> allRatingUsers = ratingsService.getRatingUsers(result.getProductId());

       // Stream<String> usersToSendNotification = Stream.concat(allCommentingUsers.stream(), allRatingUsers.stream());
       // usersToSendNotification.forEach(email -> emailNotificationService.sendEmailNotification(email, MESSAGE));
    }
}
