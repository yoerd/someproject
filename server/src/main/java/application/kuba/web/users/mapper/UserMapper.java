package application.kuba.web.users.mapper;

import application.kuba.web.users.model.UserModel;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface UserMapper {

    @Insert("insert into users(username,email,password,role,enabled) values(#{username},#{email},#{password},#{role},#{enabled})")
    int insertUser(UserModel userModel);

    @Select("select * from users WHERE username=#{username}")
    UserModel findByUsername(String username);
}
