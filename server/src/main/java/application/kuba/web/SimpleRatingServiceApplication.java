package application.kuba.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleRatingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleRatingServiceApplication.class, args);
	}
}
