package application.kuba.web.categories.mapper;

import application.kuba.web.categories.model.Category;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryMapper {

    @Select("select t.* from film_categories t join films p on p.CATEGORY_FK = t.id where p.id = #{id}")
    Category getTypeByProductId(long id);

    @Select("select * from film_categories")
    List<Category> fetchAllTypes();
}
