package application.kuba.web.categories.service.impl;

import application.kuba.web.categories.mapper.CategoryMapper;
import application.kuba.web.categories.model.Category;
import application.kuba.web.categories.service.CategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    private CategoryMapper categoryMapper;

    public CategoryServiceImpl(CategoryMapper categoryMapper) {
        this.categoryMapper = categoryMapper;
    }

    @Override
    public List<Category> getAllTypes() {
        return categoryMapper.fetchAllTypes();
    }
}
