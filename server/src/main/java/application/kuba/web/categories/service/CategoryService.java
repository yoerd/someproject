package application.kuba.web.categories.service;

import application.kuba.web.categories.model.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getAllTypes();
}
