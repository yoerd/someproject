import {Component, OnInit} from '@angular/core';
import {FilmService} from '../../../film/service/film.service';
import {FetchData} from '../../../models/fetch-data';
import {FilmListModel} from '../../../models/film-list-model';
import {api, environment} from '../../../../environments/environment';
import {Router} from '@angular/router';

@Component({
  selector: 'app-film-list',
  templateUrl: './film-list.component.html',
  styleUrls: ['./film-list.component.scss']
})
export class FilmListComponent implements OnInit {

  films: Array<FilmListModel>;
  allLoaded: boolean;
  searchText: string;
  searchDelay;

  constructor(private filmService: FilmService,
              private router: Router) {
    this.films = [];
    this.searchText = '';
  }

  ngOnInit() {
    this.fetchFilms();
  }

  fetchFilms() {
    const fetchData: FetchData = {
      limit: environment.INFINITY_SCROLL_LIMIT,
      offset: this.films.length,
      searchText: this.searchText
    };

    this.filmService.fetchFilms(fetchData)
      .subscribe((films: FilmListModel[]) => {
          this.films = this.films.concat(films);

          if (films.length < environment.INFINITY_SCROLL_LIMIT) {
            this.allLoaded = true;
          }
        }
      );
  }

  search() {
    clearTimeout(this.searchDelay);
    this.searchDelay = setTimeout(() => {
      this.films = [];
      this.fetchFilms();
    }, 500);
  }

  goToDetails(film: FilmListModel) {
    this.router.navigate([`/film-details/${film.id}`]);
  }

  onScroll() {
    this.fetchFilms();
  }
}
