import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FilmComponent} from './component/film/film.component';
import {MatButtonModule, MatDialogModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatTabsModule} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EditFilmComponent} from './component/film-edit/edit-film.component';
import {PreviewFilmComponent} from './component/film-preview/preview-film.component';
//import {NewTypePopupComponent} from './component/include/new-type-popup/new-type-popup.component';
import {GlobalModule} from '../global/global.module';

@NgModule({
  imports: [
    CommonModule,
    MatTabsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatFormFieldModule,
    MatButtonModule,
    GlobalModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatDialogModule,
    TranslateModule
  ],
  declarations: [
    FilmComponent,
    EditFilmComponent,
    PreviewFilmComponent,
   // NewTypePopupComponent
  ],
  entryComponents: [
   // NewTypePopupComponent
  ]
})
export class FilmModule {
}
