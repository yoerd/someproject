import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {api, environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {Film} from '../../models/film';
import {Category} from '../../models/category';
import {FetchData} from '../../models/fetch-data';
import {FilmListModel} from '../../models/film-list-model';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  constructor(private httpClient: HttpClient) { }

  createFilm(form: FormData): Observable<Film> {
    return this.httpClient.post<Film>(`${environment.SERVER_ADDRESS}${api.FILM.NEW_FILM}`, form);
  }

  getAllCategories(): Observable<Array<Category>> {
    return this.httpClient.get<Array<Category>>(`${environment.SERVER_ADDRESS}${api.FILM.ALL_CATEGORIES}`);
  }

  getFilmById(filmId: number): Observable<Film> {
    return this.httpClient.get<Film>(`${environment.SERVER_ADDRESS}${api.FILM.FILM}${filmId}`);
  }

  fetchFilms(fetchData: FetchData): Observable<Array<FilmListModel>> {
    return this.httpClient.post<Array<FilmListModel>>(`${environment.SERVER_ADDRESS}${api.FILM.FILMS}`, fetchData);
  }
}

