import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Film} from '../../../models/film';
import {Router} from '@angular/router';
import {Role} from '../../../models/role';
import {AuthService} from '../../../auth/auth.service';

@Component({
  selector: 'app-preview-film',
  templateUrl: './preview-film.component.html',
  styleUrls: ['./preview-film.component.scss']
})
export class PreviewFilmComponent implements OnInit {

  @Input('film') film: Film;
  @Output() switchEditMode: EventEmitter<boolean> = new EventEmitter();

  constructor(private authService: AuthService,
              private router: Router) {
  }

  ngOnInit() {
  }

  editFilm() {
    this.switchEditMode.emit(true);
    this.router.navigate(['/film-details/', this.film.id, 'edit']);
  }

  isAdmin(): boolean {
    if (!this.authService.userData) {
      return false;
    }

    return this.authService.userData.role === Role.ROLE_ADMIN;
  }
}
