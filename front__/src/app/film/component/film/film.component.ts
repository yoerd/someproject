import {Component, OnInit} from '@angular/core';
import {Film} from '../../../models/film';
import {ActivatedRoute, Router} from '@angular/router';
import {FilmService} from '../../service/film.service';
import {FilmListModel} from '../../../models/film-list-model';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.scss']
})
export class FilmComponent implements OnInit {

  film: Film;
  editMode: boolean;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private filmService: FilmService) {


    this.route.params.subscribe(params => {
      const filmId: number = params.id;
      this.editMode = params.edit ? true : false;

      if (filmId) {
        this.loadFilmData(filmId);
      } else {
        this.film = new Film();
      }
    });
  }

  ngOnInit() {

  }

  loadFilmData(filmId: number) {
    this.filmService.getFilmById(filmId)
      .subscribe((film: Film) => {
        this.film = film;
      }, (err) => {
        console.log(err);
        if (err.status = 404) {
          this.notFoundRedirect();
        }
      });
  }

  notFoundRedirect() {
    this.router.navigate([`/404`]);
  }

  switchEditMode(value: boolean) {
    this.editMode = value;
  }
}
