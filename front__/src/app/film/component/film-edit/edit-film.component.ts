import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Film} from '../../../models/film';
import {Category} from '../../../models/category';
import {FormControl, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {FilmService} from '../../service/film.service';
import {Router} from '@angular/router';
import {api, environment} from '../../../../environments/environment';

@Component({
  selector: 'app-edit-film',
  templateUrl: './edit-film.component.html',
  styleUrls: ['./edit-film.component.scss']
})
export class EditFilmComponent implements OnInit {

  @Input('film') film: Film;
  @Output() switchEditMode: EventEmitter<boolean> = new EventEmitter();

  categories: Array<Category>;

  categoryControl = new FormControl('', [Validators.required]);
  
  nameControl = new FormControl('', [Validators.required]);

  constructor(private dialog: MatDialog,
              private router: Router,
              private filmService: FilmService) {
  }

  ngOnInit() {
    this.getAllCategory();
  }


  createFilm() {
    const formData: FormData = new FormData();
    formData.append('film', JSON.stringify(this.film));

    this.filmService.createFilm(formData)
      .subscribe((film: Film) => {
        this.router.navigate(['/film-details/', film.id]);
      });
  }

  getAllCategory() {
    return this.filmService.getAllCategories()
      .subscribe((category: Array<Category>) => {
        this.categories = category;
      });
  }

  isFormValid() {
    return this.nameControl.valid && this.categoryControl.valid;
  }
}
