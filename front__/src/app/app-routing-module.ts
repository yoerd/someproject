import {FilmComponent} from './film/component/film/film.component';
import {Route, RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {FilmListComponent} from './film-list/components/film-list/film-list.component';
import {NotFoundComponent} from './global/not-found/not-found.component';

const APP_ROUTES: Route[] = [
  {path: '', pathMatch: 'full', redirectTo: 'films'},
  {path: 'film/:edit', component: FilmComponent},
  {path: 'films', component: FilmListComponent},
  {path: 'film-details/:id', component: FilmComponent},
  {path: 'film-details/:id/:edit', component: FilmComponent},
  {path: '404', component: NotFoundComponent},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(APP_ROUTES)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
