import {Category} from './category';

export class Film {
  id: number;
  name: string;
  descriptions: string;
  category: Category;
  opinion: String;
  director: string;


  constructor() {
    this.id = -1;
    this.name = '';
    this.descriptions = '';
    this.category = null;
    this.opinion = '';
    this.director = '';
  }
}
