export class FilmListModel {
  id: number;
  name: string;
  descriptions: string;
}
